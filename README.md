# Dasar Python

## Mengakses File
```py
lokasi_file = "/isi/dengan/lokasi/dari/file/misal/kucing.jpg"
with open(lokasi_file, 'r') as f:
    data = f.read()
    print(data)
```

## Course Terkait
- [Python - Tutorialspoint](https://www.tutorialspoint.com/python/index.htm)
- [🦓 Python menggunakan Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/)
